> A RESTful API running on AWS Lambda using NodeJS with the Sails.js framework.

## Technologies used
  1. `awscli` >= 1.15.4
  1. `NodeJS` >= 8.10
  1. [`yarn`](https://yarnpkg.io) >= 1.7.0
  1. MongoDb >= 3.6

## Quickstart for developers
Firstly, let's start a MongoDB
```bash
# run a mongo DB
docker run -p 27017:27017 --name wow-mongo -d mongo
export MONGO_URL='mongodb://localhost/wow'
```
If this is the first run, you'll need to install the dependencies and make sure the `sails` command line tool is availble
```bash
npm i
npm i -g sails
```
Now we can start the app
```bash
sails lift
# or, if you don't want to install the sails command
node app.js
# or, if you're a developer and you want auto-reload on code changes
yarn run start:watch
```

It's running! Let's create some records, you'll need to open another terminal because sails is still running in the foreground in the other one
```bash
npm i superagent
node populate-server.js
```

...and we can get some records back
```bash
curl 'http://localhost:1337/census' # get all censuses
curl 'http://localhost:1337/user' # get all users
curl 'http://localhost:1337/census?name=something' # search for a census with matching name field
```

By default, the responses eager-load (populate in Sails Waterline terms) all fields to 2 levels deep; the current object and all primitive on linked items. If you don't want this and would prefer just IDs of linked records, you can provide the `?populate=false` query string param, for example:
```bash
curl 'http://localhost:1337/census?populate=false'
```

## Deployment
This project is designed to run on AWS Lambda. Deployment happens as part of the CircleCI build. Read more in [./.circleci/README.md](./.circleci/README.md)

## Swagger documentation
The Swagger definition is generated as Sails lifts. Once the server is running, you can access it with:
```bash
curl localhost:1337/swagger.json
```

