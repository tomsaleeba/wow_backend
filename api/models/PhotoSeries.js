module.exports = {
  attributes: {
    top: {
      model: 'photo',
      required: true
    },
    front: {
      model: 'photo',
      required: true
    },
    leaf: {
      model: 'photo',
      required: true
    },
    microHabitat: {
      model: 'photo',
      required: true
    },
    individualRecord: {
      model: 'IndividualRecord'
    },
    populationRecord: {
      model: 'PopulationRecord'
    },
  },
  versionConfig: {
    versions: ['v1'],
    vendorPrefix: 'vnd.org.wildorchidwatch.photoseries',
  }
}

