const orchidTypes = require('../util/orchidTypes.js')
const landuseTypes = require('../util/landuseTypes.js').codes
const landformElementTypes = require('../util/landformElementTypes.js').codes
const phenologyTypes = require('../util/phenologyTypes.js').codes
const floralVisitorTypes = require('../util/floralVisitorTypes.js').codes
const disturbanceAndThreatTypeCodes = require('../util/disturbanceAndThreatTypes.js').codes

module.exports = {
  attributes: {
    orchidType: {
      type: 'string',
      required: true,
      isIn: orchidTypes,
    },
    orchidPhotos: {
      collection: 'PhotoSeries',
      via: 'individualRecord'
    },
    habitatPhotos: {
      collection: 'Photo',
      via: 'individualRecord'
    },
    geojsonPoint: {
      type: 'json',
      required: true,
    },
    altitudeMetres: {
      type: 'number',
      required: true,
      min: -1000,
      max: 8000,
    },
    locationAccuracy: {
      type: 'number',
      required: false,
      min: 0,
      max: 5000,
    },
    surroundingLanduse: {
      type: 'string',
      required: true,
      isIn: landuseTypes,
    },
    isSurroundedByLitter: {
      type: 'boolean',
      required: true
    },
    hostTreeSpecies: {
      type: 'string',
      // only required for orchidType === epiphyte
    },
    epiphyteHeightCm: {
      type: 'number',
      min: 0,
      max: 9999
    },
    hostTreePhoto: {
      model: 'photo',
    },
    landformElement: {
      type: 'string',
      required: true,
      isIn: landformElementTypes,
    },
    isSoilSandy: {
      type: 'boolean',
      required: true
    },
    vegetationStructuralType: {
      type: 'string',
      // isIn: [TODO get NVIS 2-5 values]
    },
    vegetationCommunityNotes: {
      type: 'string',
      maxLength: 50000
    },
    signOfDisturbanceAndThreats: {
      type: 'string',
      isIn: disturbanceAndThreatTypeCodes,
    },
    phenology: {
      type: 'string',
      isIn: phenologyTypes,
    },
    floralVisitorsObserved: {
      type: 'string',
      isIn: floralVisitorTypes,
    },
    floralVisitorsPhoto: {
      model: 'photo',
    },
    isDamagedFlowers: {
      type: 'boolean'
    },
    isDeheadedFlowers: {
      type: 'boolean'
    },
    census: {
      collection: 'Census',
      via: 'individualRecord'
    },
    identifications: {
      collection: 'Identification',
      via: 'individualRecord'
    }
  },
  beforeCreate: function (inputs, cb) {
    // TODO validate hostTreeSpecies when required
    // TODO validate geojsonPoint is valid GeoJSON and in Australia
    return cb()
  },
  versionConfig: {
    versions: ['v1'],
    vendorPrefix: 'vnd.org.wildorchidwatch.record.individual',
  }
}
