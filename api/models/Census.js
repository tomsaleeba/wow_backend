module.exports = {
  attributes: {
    name: {
      type: 'string',
      required: true,
      maxLength: 200,
    },
    censusStartDate: {
      type: 'number',
      required: true,
      columnType: 'datetime',
      isAfter: new Date('1900-01-01T00:00:00'),
      isBefore: new Date('2100-12-30T23:59:59'),
    },
    censusEndDate: {
      type: 'number',
      required: false,
      columnType: 'datetime',
      isAfter: new Date('1900-01-01T00:00:00'),
      isBefore: new Date('2100-12-30T23:59:59'),
    },
    type: {
      type: 'string',
      required: true,
      isIn: ['individual', 'population', 'mapping'],
    },
    individualRecord: {
      model: 'IndividualRecord'
    },
    populationRecord: {
      model: 'PopulationRecord'
    },
    mappingRecord: {
      model: 'MappingRecord'
    },
    deviceInfo: {
      collection: 'DeviceInfo',
      via: 'census',
    },
    collectedBy: {
      model: 'User'
    },
  },
  beforeCreate: function (values, cb) {
    if (!values.censusEndDate) {
      values.censusEndDate = values.censusStartDate
    }
    return cb()
  },
  versionConfig: {
    versions: ['v1'],
    vendorPrefix: 'vnd.org.wildorchidwatch.census',
  }
}
