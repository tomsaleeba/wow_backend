module.exports = {
  attributes: {
    habitatPhotos: {
      collection: 'photo',
      via: 'mappingRecord'
    },
    geojsonLineString: { // LineString https://macwright.org/2015/03/23/geojson-second-bite.html#linestrings
      type: 'json',
      required: true,
    },
    startAltitudeMetres: { // altitude at first point
      type: 'number',
      required: true,
      min: -1000,
      max: 8000,
    },
    locationAccuracy: {
      type: 'number',
      required: false,
      min: 0,
      max: 5000,
    },
    census: {
      collection: 'Census',
      via: 'mappingRecord'
    }
  },
  beforeCreate: function (values, cb) {
    // TODO validate geojsonLineString is valid GeoJSON and in Australia
    return cb()
  },
  versionConfig: {
    versions: ['v1'],
    vendorPrefix: 'vnd.org.wildorchidwatch.record.mapping',
  }
}

