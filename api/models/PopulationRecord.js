const orchidTypes = require('../util/orchidTypes.js')
const landuseTypes = require('../util/landuseTypes.js').codes
const landformElementTypes = require('../util/landformElementTypes.js').codes
const phenologyTypes = require('../util/phenologyTypes.js').codes
const floralVisitorTypes = require('../util/floralVisitorTypes.js').codes
const disturbanceAndThreatTypeCodes = require('../util/disturbanceAndThreatTypes.js').codes

module.exports = {
  attributes: {
    orchidType: {
      type: 'string',
      required: true,
      isIn: orchidTypes,
    },
    orchidPhotos: {
      collection: 'PhotoSeries',
      via: 'populationRecord'
    },
    habitatPhotos: {
      collection: 'Photo',
      via: 'populationRecord'
    },
    geojsonPolygon: {
      type: 'json',
      required: true,
    },
    altitudeMetres: {
      type: 'number',
      required: true,
      min: -1000,
      max: 8000,
    },
    locationAccuracy: {
      type: 'number',
      required: false,
      min: 0,
      max: 5000,
    },
    surroundingLanduse: {
      type: 'string',
      required: true,
      isIn: landuseTypes,
    },
    isSurroundedByLitter: {
      type: 'boolean',
      required: true
    },
    hostTreeSpecies: {
      type: 'string',
      // only required for orchidType === epiphyte
    },
    epiphyteHeightCm: {
      type: 'number',
      min: 0,
      max: 9999
    },
    hostTreePhoto: {
      model: 'photo',
    },
    landformElement: {
      type: 'string',
      required: true,
      isIn: landformElementTypes
    },
    isSoilSandy: {
      type: 'boolean',
      required: true
    },
    numberOfIndividualsRecorded: {
      type: 'number',
      required: true,
    },
    accuracyOfCount: {
      type: 'string',
      required: true,
      isIn: ['exact', 'estimate'],
    },
    approxAreaSearched: {
      type: 'string',
      isIn: ['whole', 'subset'],
    },
    vegetationStructuralType: {
      type: 'string',
      // isIn: [TODO get NVIS 2-5 values]
    },
    vegetationCommunityNotes: {
      type: 'string',
      maxLength: 50000
    },
    signOfDisturbanceAndThreats: {
      type: 'string',
      isIn: disturbanceAndThreatTypeCodes,
    },
    phenology: {
      type: 'string',
      isIn: phenologyTypes,
    },
    floralVisitorsObserved: {
      type: 'string',
      isIn: floralVisitorTypes,
    },
    census: {
      collection: 'Census',
      via: 'populationRecord'
    },
    identifications: {
      collection: 'Identification',
      via: 'populationRecord'
    }
  },
  beforeCreate: function (inputs, cb) {
    // TODO validate hostTreeSpecies when required
    // TODO validate geojsonPolygon is a polygon and in Australia
    return cb()
  },
  versionConfig: {
    versions: ['v1'],
    vendorPrefix: 'vnd.org.wildorchidwatch.record.population',
  }
}
