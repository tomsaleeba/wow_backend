module.exports = {
  attributes: {
    name: {
      type: 'string',
      required: true,
      maxLength: 200,
    },
    manufacturer: {
      type: 'string',
      required: true,
      maxLength: 200,
    },
    model: {
      type: 'string',
      required: true,
      maxLength: 200,
    },
    version: {
      type: 'string',
      required: true,
      maxLength: 200,
    },
    displayDensity: {
      type: 'string',
      maxLength: 50,
    },
    displayDpi: {
      type: 'number',
    },
    displayHeight: {
      type: 'number',
    },
    displayWidth: {
      type: 'number',
    },
    logicalDensityFactor: {
      type: 'number',
    },
    census: {
      model: 'Census',
    },
  },
  versionConfig: {
    versions: ['v1'],
    vendorPrefix: 'vnd.org.wildorchidwatch.deviceinfo',
  }
}
