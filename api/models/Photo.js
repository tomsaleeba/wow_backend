const photoTypes = require('../util/photoTypes.js').codes

module.exports = {
  attributes: {
    name: {
      type: 'string',
      required: true,
      maxLength: 200,
    },
    data: {
      type: 'string',
      required: true,
      maxLength: 10 * 1024 * 1024 // 10MB, in case we're inlining the data
    },
    type: {
      type: 'string',
      required: true,
      isIn: photoTypes,
    },
    exif: {
      type: 'json',
    },
    individualRecord: {
      model: 'IndividualRecord'
    },
    mappingRecord: {
      model: 'MappingRecord'
    },
    populationRecord: {
      model: 'PopulationRecord'
    },
  },
  versionConfig: {
    versions: ['v1'],
    vendorPrefix: 'vnd.org.wildorchidwatch.photo',
  }
}
