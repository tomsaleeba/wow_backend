/* A determination made by an expert.
 * We keep an audit trail so we never delete them. For display, use the newest entry.
 */
module.exports = {
  attributes: {
    determinedName: {
      type: 'string',
      required: true,
      maxLength: 500
    },
    eventTimestamp: {
      type: 'number',
      required: true,
      columnType: 'datetime',
      isAfter: new Date('1900-01-01T00:00:00'),
      isBefore: new Date('2100-12-30T23:59:59'),
    },
    // TODO do we need a single linked list structure that points to the previous entry?
    identifiedBy: {
      model: 'User'
    },
    populationRecord: {
      model: 'PopulationRecord'
    },
    individualRecord: {
      model: 'IndividualRecord'
    },
  },
  versionConfig: {
    versions: ['v1'],
    vendorPrefix: 'vnd.org.wildorchidwatch.identification',
  }
}
