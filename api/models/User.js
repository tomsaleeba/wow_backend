module.exports = {
  attributes: {
    name: {
      type: 'string',
      required: true,
      maxLength: 100
    },
    email: {
      type: 'string',
      required: false,
      isEmail: true,
      maxLength: 200,
    },
    isSighter: {
      type: 'boolean',
      defaultsTo: false,
    },
    isIdentifier: {
      type: 'boolean',
      defaultsTo: false,
    },
    isAdmin: {
      type: 'boolean',
      defaultsTo: false,
    },
    collectorOf: {
      collection: 'Census',
      via: 'collectedBy',
    },
  },
  versionConfig: {
    versions: ['v1'],
    vendorPrefix: 'vnd.org.wildorchidwatch.user',
  }
}
