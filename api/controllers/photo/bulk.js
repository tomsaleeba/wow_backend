module.exports = {
  friendlyName: 'Photo bulk update',
  description: 'An easy way for a client to bulk update fields on recently created records',

  inputs: {
    updates: {
      type: [{
        datastoreId: 'string', // the object ID that tus generates, probably the S3 object name too
        census: 'string',
        point: 'number',
        sequence: 'number',
        exif: 'ref',
      }],
      required: true,
    },
  },

  exits: {
    success: {
      statusCode: 201,
    },
    internalServerError: {
      responseType: 'serverError',
      statusCode: 500
    }
  },

  fn: async function (inputs, exits) {
    // TODO this should probably be versioned along with all other Photo actions
    try {
      for (const curr of inputs.updates) {
        const apiBaseUrl = `${this.req.protocol}://${this.req.hostname}:${this.req.port}`
        const imageUrl = sails.helpers.getDatastoreStrategy().getImageUrl(curr.datastoreId, apiBaseUrl)
        const patch = {
          data: imageUrl,
          type: curr.type, // FIXME do we only allow uploading one type at a time?
          exif: curr.exif,
        }
        sails.log.debug(`[photo.bulk] updating record with datastoreId='${curr.datastoreId}' using patch=${JSON.stringify(patch, null, 2)}`)
        await sails.models.photo.update({
          data: curr.datastoreId,
        }, patch)
      }
      // FIXME dynamically look up this MIME and probably extract to a helper
      const photoVersionConfig = sails.models.photo.versionConfig
      const mime = `application/${photoVersionConfig.vendorPrefix}.${photoVersionConfig.versions[0]}+json`
      this.res.set('Content-type', mime)
      return exits.success({success: true})
    } catch (err) {
      return exits.internalServerError(err)
    }
  }
}
