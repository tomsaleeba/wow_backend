module.exports = {
  inputs: {
    id: {
      type: 'string',
      required: true
    }
  },
  exits: {
    serverError: {
      responseType: 'serverError'
    },
    notFound: {
      responseType: 'notFound'
    }
  },

  fn: async function (inputs, exits) {
    const res = this.res
    return sails.helpers.getDatastoreStrategy().getFromUppy(inputs, exits, res)
  },
}
