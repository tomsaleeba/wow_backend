const S3 = require('aws-sdk/clients/s3')
const fs = require('fs')
const nodePath = require('path')
const tusMetadata = require('tus-metadata')
const fileType = require('file-type')
const tus = require('tus-node-server')
const assert = require('assert')

const s3LogPrefix = '[S3 strategy]'
const fileLogPrefix = '[file strategy]'

const s3Strategy = {
  init: function () { },
  getFromUppy: function (inputs, exits, res) {
    const s3client = getS3Client()
    const params = {
      Bucket: sails.config.custom.awsBucket,
      Key: inputs.id,
    }
    s3client.getObject(params, (err, data) => {
      if (err) {
        if (err.statusCode === 403 || err.statusCode === 404) {
          return exits.notFound()
        }
        sails.log.error(err, err.stack)
        return exits.serverError({
          success: false,
          status: 500,
        })
      }
      const s3Metadata = data.Metadata
      const tusUploadMetadata = tusMetadata.decode(s3Metadata.upload_metadata)
      res
        .set('Content-length', s3Metadata.upload_length)
        .set('Content-type', tusUploadMetadata.type)
        .send(data.Body)
      // yeah, we don't call exits.<something>() here because we've already sent the response
    })
  },
  onUploadComplete: function (filename, fileId) {
    const s3Client = getS3Client()
    const params = {
      Bucket: sails.config.custom.awsBucket,
      Key: fileId,
      ACL: 'public-read',
    }
    s3Client.putObjectAcl(params, (err) => {
      if (err) {
        sails.log.error(`${s3LogPrefix} Failed to set public-read ACL for ${filename}, with error: ${JSON.stringify(err, null, 2)}`)
        return
      }
      sails.log.debug(`${s3LogPrefix} successfully set public-read ACL for ${fileId} (${filename})`)
    })
  },
  getImageUrl: function (filename/*, apiBaseUrl*/) {
    return `https://${sails.config.custom.awsBucket}.s3.amazonaws.com/${filename}`
  },
  configureTusServer: function (tusServer) {
    const awsAccessKey = sails.config.custom.awsAccessKey
    const awsSecretAccessKey = sails.config.custom.awsSecretAccessKey
    const awsBucket = sails.config.custom.awsBucket
    const awsRegion = sails.config.custom.awsRegion
    assert.ok(awsAccessKey, 'AWS "access key id" must be set when tusDatastore === "s3", check config/custom.js')
    assert.ok(awsSecretAccessKey, 'AWS "secret access key" must be set when tusDatastore === "s3", check config/custom.js')
    assert.ok(awsBucket, 'AWS "bucket" must be set when tusDatastore === "s3", check config/custom.js')
    assert.ok(awsRegion, 'AWS "region" must be set when tusDatastore === "s3", check config/custom.js')
    console.log(`Using S3 as tus datastore with bucket=${awsBucket} in region=${awsRegion}`)

    tusServer.datastore = new tus.S3Store({
      path: sails.config.custom.tusServerRoute,
      bucket: awsBucket,
      accessKeyId: awsAccessKey,
      secretAccessKey: awsSecretAccessKey,
      region: awsRegion,
      partSize: 8 * 1024 * 1024, // each uploaded part will have ~8MB,
    })
  },
  storeImage: function (imageBase64Data, filename, recordName/*, apiBaseUrl*/) {
    const s3Client = getS3Client()
    const imageBytes = new Buffer(imageBase64Data, 'base64')
    const imageFileType = fileType(imageBytes)
    const mimeType = imageFileType.mime
    let filenameForHeader = recordName
    if (!filenameForHeader.endsWith(imageFileType.ext)) {
      filenameForHeader = `${filenameForHeader}.${imageFileType.ext}`
    }
    const params = {
      ACL: 'public-read',
      Body: imageBytes,
      Bucket: sails.config.custom.awsBucket,
      ContentDisposition: `filename="${filenameForHeader}"`,
      ContentType: mimeType,
      Key: filename,
      Metadata: {
        recordName: recordName,
      },
    }
    return new Promise((resolve, reject) => {
      s3Client.putObject(params, err => {
        if (err) {
          return reject(err)
        }
        const imageUrl = s3Strategy.getImageUrl(filename)
        return resolve(imageUrl)
      })
    })
  },
}
const publicAssetDir = '.tmp/public'
const brittleHackToGetProjectRoot = '.'
const fileStrategy = { // only intended for local dev
  publicAssetSubdir: 'uploads', // don't set to '' (0 length string)
  init: function () {
    const path = nodePath.join(publicAssetDir, this.publicAssetSubdir)
    if (fs.existsSync(path)) {
      return
    }
    fs.mkdirSync(path)
  },
  getFromUppy: function (inputs, exits, res) {
    fs.readFile(nodePath.join(brittleHackToGetProjectRoot, sails.config.custom.tusServerRoute, inputs.id), (err, data) => {
      if (err) {
        sails.log.debug(err, err.stack)
        return exits.notFound()
      }
      res
        .set('Content-length', data.length)
        .set('Content-type', fileType(data))
        .send(data)
      // yeah, we don't call exits.<something>() here because we've already sent the response
    })
  },
  onUploadComplete: function (filename, fileId) {
    const src = nodePath.join(brittleHackToGetProjectRoot, sails.config.custom.tusServerRoute, fileId)
    const dest = nodePath.join(publicAssetDir, this.publicAssetSubdir, fileId)
    fs.copyFileSync(src, dest)
  },
  getImageUrl: function (filename, apiBaseUrl) {
    return `${apiBaseUrl}/${this.publicAssetSubdir}/${filename}`
  },
  configureTusServer: function (tusServer) {
    console.log(`${fileLogPrefix} Using "file/disk" as tus datastore with path=${sails.config.custom.tusServerRoute}`)
    tusServer.datastore = new tus.FileStore({
      path: sails.config.custom.tusServerRoute,
    })
  },
  storeImage: function (imageBase64Data, filename, recordName, apiBaseUrl) {
    const path = nodePath.join(publicAssetDir, this.publicAssetSubdir, filename)
    fs.appendFileSync(path, new Buffer(imageBase64Data, 'base64'))
    const result = this.getImageUrl(filename, apiBaseUrl)
    return Promise.resolve(result)
  },
}

const machineDef = {
  inputs: {},
  sync: true,
  fn: function (_, exits) {
    const tusDatastore = sails.config.custom.tusDatastore
    const result = machineDef._strategies[tusDatastore]
    if (!result) {
      throw new Error(`Programmer error: no strategy found where tusDatastore='${tusDatastore}'`)
    }
    result.init()
    return exits.success(result)
  },
  _strategies: {
    s3: s3Strategy,
    file: fileStrategy,
  },
}

module.exports = machineDef

function getS3Client () {
  const result = new S3()
  const isNotRunningInLambda = !process.env.AWS_LAMBDA_FUNCTION_VERSION
  if (isNotRunningInLambda) {
    // FIXME this is non-API but it seems to work
    result.config.credentials.accessKeyId = sails.config.custom.awsAccessKey
    result.config.credentials.secretAccessKey = sails.config.custom.awsSecretAccessKey
  }
  return result
}
