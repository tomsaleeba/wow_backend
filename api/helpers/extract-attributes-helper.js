module.exports = {
  friendlyName: 'Extract attributes helper',

  description: 'Takes a Waterline model attribute object and transforms it to be used as the "inputs" for a controller function, useful for making facades',

  inputs: {
    modelDef: {
      type: {},
      description: 'waterline model definition (just pass the require()d model file)',
      required: true
    }
  },

  sync: true,

  fn: function (inputs, exits) {
    // modelIdType = using sails-mongo ? 'string' : 'number' (for sails-disk)
    const modelIdType = sails.config.models.attributes.id.type // might break if you override 'id' on a model
    const attrs = inputs.modelDef.attributes
    const result = Object.keys(attrs).reduce((accum, curr) => {
      let value = attrs[curr]
      const isOneToMany = value.collection
      if (isOneToMany) {
        accum[curr] = { type: [modelIdType], required: false }
        return accum
      }
      const isOneToOne = value.model
      if (isOneToOne) {
        accum[curr] = { type: modelIdType, required: false }
        return accum
      }
      accum[curr] = { type: value.type, required: value.required || false }
      return accum
    }, {})
    return exits.success(result)
  }
}
