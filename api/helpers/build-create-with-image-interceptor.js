const uuidv1 = require('uuid/v1')

module.exports = {
  friendlyName: 'Build a "create" controller function',
  description: 'The built function will create the specified model but pull the binary image data out to a separate storage location',

  inputs: {
    modelFileNameFragment: {
      type: 'string',
      description: 'name of the file containing the model definition. e.g: for Blah.js, supply "Blah"',
      required: true
    },
  },

  sync: true,

  fn: function (builderInputs, builderExits) {
    const modelFileNameFragment = builderInputs.modelFileNameFragment
    const result = {
      inputs: sails.helpers.extractAttributesHelper(require(`../models/${modelFileNameFragment}`)),
      exits: {
        success: {
          // doing this causes the custom response handler in
          // sails-hook-api-version-accept to be called because we
          // hit this case: https://github.com/sailshq/machine-as-action/blob/v10.1.1/lib/machine-as-action.js#L1075
          // and not this one: https://github.com/sailshq/machine-as-action/blob/v10.1.1/lib/machine-as-action.js#L761
          responseType: 'ok',
        },
        serverError: {
          responseType: 'serverError',
        }
      },
      fn: async function (facadeInputs, facadeExits) {
        try {
          const imageBase64Data = facadeInputs.data
          const filename = uuidv1()
          const apiBaseUrl = `${this.req.protocol}://${this.req.hostname}:${this.req.port}`
          const imageUrl = await sails.helpers.getDatastoreStrategy()
            .storeImage(imageBase64Data, filename, facadeInputs.name, apiBaseUrl)
          facadeInputs.data = imageUrl
          const inputsWithDataUrl = facadeInputs
          const result = await sails.models[modelFileNameFragment.toLowerCase()]
            .create(inputsWithDataUrl)
            .fetch()
          return facadeExits.success(result)
        } catch (err) {
          return facadeExits.serverError(err)
        }
      }
    }

    return builderExits.success(result)
  }
}
