const flaverr = require('flaverr')

module.exports = {
  friendlyName: 'Bad request helper',

  description: 'Aids in the creation of an error that will cause beforeCreate to return a 400',

  inputs: {
    details: {
      type: 'string',
      description: 'message that tells the caller what went wrong',
      require: true
    }
  },

  fn: async function (inputs, exits) {
    const result = flaverr({
      name: 'UsageError',
      details: inputs.details
    })
    return exits.success(result)
  }
}
