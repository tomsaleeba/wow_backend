/**
 * tus hook
 *
 * @description :: binds a route for tus
 */
const tus = require('tus-node-server')
const tusMetadata = require('tus-metadata')

module.exports = function defineTusHook(sails) {

  const hook = {}
  const logPrefix = '[tus hook]'

  return {

    initialize: function (done) {
      const server = new tus.Server()
      sails.helpers.getDatastoreStrategy().configureTusServer(server)

      server.on(tus.EVENTS.EVENT_UPLOAD_COMPLETE, event => {
        const metadata = tusMetadata.decode(event.file.upload_metadata)
        sails.log.debug(`${logPrefix} Upload complete, via tus protocol, for file ${event.file.id} with name ${metadata.filename}`)
        sails.models.photopanorama.create({
          name: metadata.filename,
          data: event.file.id,
          point: 0,
          sequence: 0,
          source: 'dslr',
          isStitched: false,
        }).then(() => {
          sails.log.debug(`${logPrefix} Created panorama record for ${metadata.filename}`)
        }).catch(err => {
          sails.log.error(`${logPrefix} Failed to create record for ${metadata.filename}, with error: ${JSON.stringify(err, null, 2)}`)
        })
        sails.helpers.getDatastoreStrategy().onUploadComplete(metadata.filename, event.file.id)
      })

      hook.handler = (req, res, next) => {
        // we play with baseUrl otherwise locally we get a URL that looks like: //localhost:1337http://localhost:1337/files/d1a99d30cf6b580b5d8683cd84c398b6
        // and on Lambda we lose the stage fragment because it's no passed to us in any of the "normal" places.
        req.baseUrl = null
        // we need to set this baseUrl to be the staging fragment so on Lambda, the correct Location header is returned from a POST:
        //    https://github.com/tus/tus-node-server/blob/5dad86d04d939548eff1cb3e01f0f05a4d9332ec/lib/handlers/PostHandler.js#L19
        // BUT, we can't set it for the PATCH and HEAD otherwise the regex that pulls the file name out fails:
        //    https://github.com/mycujoo/tus-node-server/blob/96370e1f7c14a7eb0e955dff7abc380d0ffdac40/lib/handlers/BaseHandler.js#L44
        if (req.method.toUpperCase() === 'POST' && req.headers['x-stage']) {
          // might break if we still get this header when using a custom domain
          req.baseUrl = '/' + req.headers['x-stage']
        }
        const tusHandler = server.handle.bind(server)
        return tusHandler(req, res, next)
      }

      return done()
    },

    routes: {
      before: {
        [`${sails.config.custom.tusServerRoute}*`]: function (req, res, next) {
          if (req.method === 'GET') {
            return next()
          }
          return hook.handler(req, res, next)
        },
      }
    }

  }

}
