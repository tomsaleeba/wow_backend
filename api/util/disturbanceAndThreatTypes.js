const vocab = [
  { code: 'clearing-veg', label: 'Clearing (vegetation clearance)' },
  { code: 'clearing-mow', label: 'Clearing - mowing/slashing' },
  { code: 'chem-spray', label: 'Chemical spray (incl. spray drift)' },
  { code: 'cultivation', label: 'Cultivation (incl. pasture/ag activities)' },
  { code: 'soil-erosion', label: 'Soil erosion (incl. run off)' },
  { code: 'wood-removal', label: 'Firewood/coarse woody debris removal' },
  { code: 'grazing', label: 'Grazing (native species, presence of roo/possum scats)' },
  { code: 'fire1', label: 'Fire, within past year' },
  { code: 'fire3', label: 'Fire, in past 3 years' },
  { code: 'fireN', label: 'Fire, more than 3 years ago' }, // TODO review fire granularity
  { code: 'storm', label: 'Storm damage' },
  { code: 'weed', label: 'Weed invasion' },
  { code: 'foot', label: 'Foot tramping (human)' },
  { code: 'other-human', label: 'Other human (e.g. car parking)' },
]
const codes = vocab.reduce((accum, curr) => {
  accum.push(curr.code)
  return accum
}, [])

module.exports = {
  vocab,
  codes,
}

