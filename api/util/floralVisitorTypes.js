const vocab = [
  { code: 'nbee', beginnerLabel: 'Bee', advancedLabel: 'Native bee' },
  { code: 'nwasp', beginnerLabel: 'Wasp', advancedLabel: 'Native wasp' },
  { code: 'nfly', beginnerLabel: 'Fly', advancedLabel: 'Native fly' },
  { code: 'gnat', beginnerLabel: 'Gnat', advancedLabel: 'Fungus gnat' },
  { code: 'ant', beginnerLabel: 'Ant', advancedLabel: 'Ant' },
  { code: 'unknown', beginnerLabel: 'Unknown insect', advancedLabel: 'Unknown insect' },
]
const codes = vocab.reduce((accum, curr) => {
  accum.push(curr.code)
  return accum
}, [])

module.exports = {
  vocab,
  codes,
}

