const vocab = [ // TODO get the rest of these codes
  { code: 'plain', label: 'plain' },
  { code: 'playa', label: 'playa' },
  { code: 'lunette', label: 'lunette' },
  { code: 'breakaway', label: 'breakaway' },
  { code: 'dune', label: 'dune' },
  { code: 'swale', label: 'swale' },
  { code: 'hillcrest', label: 'hillcrest' },
  { code: 'hill', label: 'hill' },
  { code: 'slope', label: 'slope' },
  { code: 'gully', label: 'gully' },
  { code: 'cliff', label: 'cliff' },
  { code: 'stream', label: 'stream' },
  { code: 'swamp', label: 'swamp' },
]
const codes = vocab.reduce((accum, curr) => {
  accum.push(curr.code)
  return accum
}, [])

module.exports = {
  vocab,
  codes,
}

