const vocab = [
  { code: 'top', label: 'Top' },
  { code: 'front', label: 'Front' },
  { code: 'leaf', label: 'Leaf' },
  { code: 'mhabitat', label: 'Micro-habitat' },
  { code: 'pollinators', label: 'Visitng pollinators' },
  { code: 'habitat', label: 'Habitat' },
]
const codes = vocab.reduce((accum, curr) => {
  accum.push(curr.code)
  return accum
}, [])

module.exports = {
  vocab,
  codes,
}

