const vocab = [
  { code: 'conservation', label: 'conservation' }, // and natural environments
  { code: 'production-natural', label: 'production-natural' }, // from relatively natural environments
  { code: 'production-dryland', label: 'production-dryland' }, // from dryland agriculture and plantations
  { code: 'production-irrigated', label: 'production-irrigated' }, // from irrigated agriculture and plantations
  { code: 'intensive', label: 'intensive' }, // uses
  { code: 'water', label: 'water' }
]
const codes = vocab.reduce((accum, curr) => {
  accum.push(curr.code)
  return accum
}, [])

module.exports = {
  vocab,
  codes,
}

