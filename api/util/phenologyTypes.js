const vocab = [
  { code: 'bud', beginnerLabel: 'Bud', advancedLabel: 'Bud' },
  { code: 'flower', beginnerLabel: 'Flower', advancedLabel: 'Flower' },
  { code: 'sen-flower', beginnerLabel: 'Senescent flower', advancedLabel: 'Senescent flower' },
  { code: 'dev-fruit', beginnerLabel: 'Developing fruit', advancedLabel: 'Developing fruit' },
  { code: 'sen-fruit', beginnerLabel: 'Senescent fruit', advancedLabel: 'Senescent fruit' },
]
const codes = vocab.reduce((accum, curr) => {
  accum.push(curr.code)
  return accum
}, [])

module.exports = {
  vocab,
  codes,
}

