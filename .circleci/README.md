# Getting set up with CircleCI
You need to configure a few things in CircleCI for this CI/CD pipeline to work.

## Environment variables
 1. [AWS credentials](https://circleci.com/docs/2.0/deployment-integrations/#aws) (`AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY`) for an account that has:
    1. access required by [Apex UP](https://up.docs.apex.sh/#aws_credentials.iam_policy_for_up_cli) to do the deployment
    1. access to read/write objects to the S3 bucket (`AWS_BUCKET` that you'll use below) configured for photo uploads
 1. `AWS_REGION` as the region the S3 bucket(s) lives in, probably `ap-southeast-2`. This affects staging *and* production buckets.
 1. `STAGING_AWS_BUCKET` as the S3 bucket name that you want **staging** photo uploads to be stored in, see below for how to create this bucket
 1. `STAGING_MONGO_URL` as the URL for the Mongo staging DB
 1. `PRODUCTION_AWS_BUCKET` as the S3 bucket name that you want **production** photo uploads to be stored in, see below for how to create this bucket
 1. `PRODUCTION_MONGO_URL` as the URL for the Mongo production DB
 1. `ROLLBAR_ACCESS_TOKEN` for sending errors to [Rollbar](https://docs.rollbar.com/reference#section-authentication) and notifying of a deployment

## Creating the S3 bucket
The bucket that you need for the `AWS_BUCKET` environmental variable above can be created using the follow `awscli` commands:

  1. define an env var for our bucket name, make this whatever you like:
      ```bash
      export AWS_BUCKET=wow-photo-uploads # change me if you like
      ```
  1. create the bucket
      ```bash
      aws s3api create-bucket \
        --bucket $AWS_BUCKET \
        --region ap-southeast-2 \
        --create-bucket-configuration LocationConstraint=ap-southeast-2
      ```
  1. create and upload an `index.html` file as you *need* one for S3 website buckets.
      ```bash
      echo '<html><body>Nothing here</body></html>' | \
        aws s3 cp - s3://$AWS_BUCKET/index.html \
        --acl public-read
      ```

  1. configure the bucket for website hosting
      ```bash
      aws s3api put-bucket-website \
        --bucket $AWS_BUCKET \
        --website-configuration '{"IndexDocument":{"Suffix":"index.html"}}'
      ```

## AWS permissions for Lambda as configured by Apex UP

Unfortunately we deal with AWS credentials in a number of ways:

  1. we read the values from `sails.config.custom` for tus
  1. when running locally, the `sails.config.custom` values are also used for the `GET` handler for photos uploaded via tus
  1. when running in Lambda, we let Lambda automatically configure the `aws-sdk`, this means the role assigned by Apex UP is active

This means the IAM role you create as defined in `sails.config.custom` *needs* write permissions to the `(STAGING|PRODUCTION)_AWS_BUCKET` S3 bucket.

When we run on Lambda, Apex UP will configure the IAM role that the Lambda runs under. `up.json` defines an IAM policy that allows the role to read from S3 buckets. At the time of writing, it allows reads *and* write from/to any object in any bucket in the account. This isn't ideal but in order to restrict it, we'll have to pre-process the `up.json` to set the bucket name before we create the stack. That's on the **TODO** list.
