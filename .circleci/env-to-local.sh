#!/usr/bin/env sh
# Converts environment variables to Sails local config.
# We're doing this because we don't have a Pro license
# for Apex UP, which you need for env support.
set -e
cd `dirname "$0"`
cd ..
: ${MONGO_URL:?}
: ${AWS_ACCESS_KEY_ID:?}
: ${AWS_SECRET_ACCESS_KEY:?}
: ${AWS_BUCKET:?}
: ${AWS_REGION:?}
: ${ROLLBAR_ENV:?}

target=config/local.js

# we need to escape the & character because that has special meaning to sed
fixedMongoUrl=`sh -c "echo '$MONGO_URL' | sed 's+&+\\\\\&+g'"`

echo "
module.exports = {
  datastores: {
    default: {
      url: '$fixedMongoUrl'
    },
  },
  custom: {
    tusDatastore: 's3',
    awsAccessKey: '$AWS_ACCESS_KEY_ID',
    awsSecretAccessKey: '$AWS_SECRET_ACCESS_KEY',
    awsBucket: '$AWS_BUCKET',
    awsRegion: '$AWS_REGION',
    swaggerJsonPath: '/tmp/swagger.json',
    rollbarEnv: '$ROLLBAR_ENV',
    rollbarAccessToken: '$ROLLBAR_ACCESS_TOKEN',
  },
}
" > $target
