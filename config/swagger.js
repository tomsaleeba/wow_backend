module.exports.swagger = {
  apiVersion: '1.0',
  swaggerVersion: '2.0',
  swaggerURL: '/api/docs',
  swaggerJSON: '/swagger.json',
  basePath: process.env.HOST_NAME || 'http://localhost:1337',
  info: {
    title: 'Wild Orchid Watch HTTP API',
    description: 'RESTful API for interacting with the WOW repository'
    //termsOfService: null, // TODO
    //contact: {
    //name: 'Andrew Tokmakoff',
    //url: 'https://bitbucket.org/atokmakoff/',
    //email: null // TODO
    //},
    //license: {
    //name: 'Apache 2.0', // TODO
    //url: 'http://www.apache.org/licenses/LICENSE-2.0.html'
    //},
  },
  externalDocs: {
    url: 'https://bitbucket.org/tomsaleeba/wow_backend'
  },
  apis: [
    //"Users.yml"
  ]
}

