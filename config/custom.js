/**
 * Custom configuration
 * (sails.config.custom)
 *
 * One-off settings specific to your application.
 *
 * For more information on custom configuration, visit:
 * https://sailsjs.com/config/custom
 */

module.exports.custom = {

  tusServerRoute: '/files', // this is *both* the filesystem path relative to project root *and* the URL prefix
  tusDatastore: process.env.TUS_DATASTORE || 'file',
  awsAccessKey: process.env.AWS_ACCESS_KEY_ID,
  awsSecretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  awsBucket: process.env.AWS_BUCKET,
  awsRegion: process.env.AWS_REGION,
  swaggerJsonPath: './swagger/swagger.json', // relative to project root
  rollbarEnv: 'dev',
  rollbarAccessToken: false, // disabled in dev
}
