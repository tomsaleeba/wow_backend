require('should')

describe('ExtractAttributesHelper tests ::', () => {
  it('should handle primitive typed attributes', () => {
    const source = {
      attributes: {
        name: {
          type: 'string'
        },
        age: {
          type: 'number'
        },
        isAlive: {
          type: 'boolean'
        },
        someJson: {
          type: 'json'
        }
      }
    }
    const result = sails.helpers.extractAttributesHelper(source)
    result.should.be.eql({
      name: {
        type: 'string',
        required: false,
      },
      age: {
        type: 'number',
        required: false,
      },
      isAlive: {
        type: 'boolean',
        required: false,
      },
      someJson: {
        type: 'json',
        required: false,
      }
    })
  })

  it('should convert 1:1 associations to their ID type', () => {
    const source = {
      attributes: {
        employer: {
          model: 'Organisation',
          required: false
        },
      }
    }
    const result = sails.helpers.extractAttributesHelper(source)
    result.should.be.eql({
      employer: {
        type: 'number',
        required: false
      }
    })
  })

  it('should convert 1:many associations to an array of the ID type', () => {
    const source = {
      attributes: {
        plots: {
          collection: 'Plot',
          via: 'studyArea',
        }
      }
    }
    const result = sails.helpers.extractAttributesHelper(source)
    result.should.be.eql({
      plots: {
        type: ['number'],
        required: false,
      }
    })
  })

})
