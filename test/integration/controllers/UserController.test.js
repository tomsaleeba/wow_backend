const supertest = require('supertest')
require('should')

describe('UserController tests ::', () => {
  describe('create', () => {
    it('should create a user by supplying the bare minimum', done => {
      supertest(sails.hooks.http.app)
        .post('/user')
        .set('Accept', '*/*')
        .send({
          name: 'Bob',
        })
        .expect(200)
        .end((err, res) => {
          if (err) {return done(err)}
          res.headers['content-type'].should.startWith('application/vnd.org.wildorchidwatch.user.v1+json')
          const body = res.body
          body.should.have.property('name').and.be.a.String().and.equal('Bob')
          done()
        })
    })
  })
})

