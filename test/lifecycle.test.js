const sails = require('sails')
const tmp = require('tmp')
tmp.setGracefulCleanup()
const tus = require('tus-node-server')
const fs = require('fs')

// Before running any tests...
before(function (done) {
  if (this.timeout() < 5000) {this.timeout(5000)}
  sails.lift({
    helpers: {
      moduleDefinitions: {
        'get-datastore-strategy': {
          sync: true,
          fn: function (_, exits) {
            return exits.success(buildStubStrategy())
          }
        },
      }
    },
    hooks: {
      'api-version-accept': true,
      grunt: false,
      views: false,
      session: false,
    },
    log: {
      level: 'warn' // change to warn, debug, etc when troubleshooting tests
    },
    models: {
      migrate: 'drop',
      attributes: {
        createdAt: { type: 'number', autoCreatedAt: true, },
        updatedAt: { type: 'number', autoUpdatedAt: true, },
        id: {
          type: 'number',
          autoMigrations: { autoIncrement: true },
          columnName: 'id', // override our config/model.js value so sails-disk doesn't complain when doing .update()
        },
      }
    },
    datastores: {
      default: {
        adapter: 'sails-disk',
        inMemoryOnly: true,
      }
    },
    local: {}, // make sure we don't read any local overrides
    custom: {
      tusServerRoute: '/files',
      tusDatastore: 'test',
      rollbarEnv: false, // disabled for tests
    },
  },(err) => {
    if (err) {return done(err)}
    return done()
  })
})

// After all tests have finished...
after((done) => {
  sails.lower(done)
})

function buildStubStrategy () {
  const tempDirPrefix = 'testTusDatastore'
  const tempDir = tmp.dirSync({
    dir: './.tmp/public/',
    prefix: tempDirPrefix,
    unsafeCleanup: true,
  })
  return {
    init: function () { },
    getFromUppy: function (/*inputs, exits, res*/) {
      // TODO probably copy helpers/get-datastore-strategy.js impl
    },
    onUploadComplete: function (/*filename, fileId*/) {
      // TODO implement me
    },
    getImageUrl: function (filename/*, apiBaseUrl*/) {
      const pathPrefix = tempDir.name.replace(/.*\//, '/')
      return `${pathPrefix}/${filename}`
    },
    configureTusServer: function (tusServer) {
      // console.log(`Test tusDatastore path='${tempDir.name}'`)
      tusServer.datastore = new tus.FileStore({
        path: tempDir.name,
      })
    },
    storeImage: function (imageBase64Data, filename, recordName, apiBaseUrl) {
      const path = `${tempDir.name}/${filename}`
      fs.appendFileSync(path, new Buffer(imageBase64Data, 'base64'))
      const urlSuffix = this.getImageUrl(filename)
      return Promise.resolve(`${apiBaseUrl}${urlSuffix}`)
    },
  }
}
