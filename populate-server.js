#!/usr/bin/env node
// Creates a set of linked records in the target instance, for easy testing.
// Run with:
//    node populate-server.js
//    node populate-server.js http://some.other/api/url
const VError = require('verror')
let request
try {
  request = require('superagent')
} catch (unused) {
  console.error('[ERROR] You need to run `npm i superagent` before running this')
  return
}

// Note: all the "Attempting to populate `some_association` with ..." errors
// that you get in the output from the API are because the API is using
// sails-mongo as the Waterline adapter and doing the POST causes a GET
// to return the new record. You can cause the same message to appear with:
//    curl 'http://localhost:1337/user'
// but if you disable the 'populate' param, you silence the message:
//    curl 'http://localhost:1337/user?populate=false'
// I can't figure out how to stop population in the triggered GET to stop
// the messages though.

const URL_PREFIX = process.argv[2] || 'http://localhost:1337'
console.log(`Running against the API with base URL: ${URL_PREFIX}`);
(async function f1 () {
  const chromeLogo = 'iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAMAAADXqc3KAAAB+FBMVEUAAAA/mUPidDHiLi5Cn0XkNTPmeUrkdUg/m0Q0pEfcpSbwaVdKskg+lUP4zA/iLi3msSHkOjVAmETdJSjtYFE/lkPnRj3sWUs8kkLeqCVIq0fxvhXqUkbVmSjwa1n1yBLepyX1xxP0xRXqUkboST9KukpHpUbuvRrzrhF/ljbwaljuZFM4jELaoSdLtElJrUj1xxP6zwzfqSU4i0HYnydMtUlIqUfywxb60AxZqEXaoifgMCXptR9MtklHpEY2iUHWnSjvvRr70QujkC+pUC/90glMuEnlOjVMt0j70QriLS1LtEnnRj3qUUXfIidOjsxAhcZFo0bjNDH0xxNLr0dIrUdmntVTkMoyfL8jcLBRuErhJyrgKyb4zA/5zg3tYFBBmUTmQTnhMinruBzvvhnxwxZ/st+Ktt5zp9hqota2vtK6y9FemNBblc9HiMiTtMbFtsM6gcPV2r6dwroseLrMrbQrdLGdyKoobKbo3Zh+ynrgVllZulTsXE3rV0pIqUf42UVUo0JyjEHoS0HmsiHRGR/lmRz/1hjqnxjvpRWfwtOhusaz0LRGf7FEfbDVmqHXlJeW0pbXq5bec3fX0nTnzmuJuWvhoFFhm0FtrziBsjaAaDCYWC+uSi6jQS3FsSfLJiTirCOkuCG1KiG+wSC+GBvgyhTszQ64Z77KAAAARXRSTlMAIQRDLyUgCwsE6ebm5ubg2dLR0byXl4FDQzU1NDEuLSUgC+vr6urq6ubb29vb2tra2tG8vLu7u7uXl5eXgYGBgYGBLiUALabIAAABsElEQVQoz12S9VPjQBxHt8VaOA6HE+AOzv1wd7pJk5I2adpCC7RUcHd3d3fXf5PvLkxheD++z+yb7GSRlwD/+Hj/APQCZWxM5M+goF+RMbHK594v+tPoiN1uHxkt+xzt9+R9wnRTZZQpXQ0T5uP1IQxToyOAZiQu5HEpjeA4SWIoksRxNiGC1tRZJ4LNxgHgnU5nJZBDvuDdl8lzQRBsQ+s9PZt7s7Pz8wsL39/DkIfZ4xlB2Gqsq62ta9oxVlVrNZpihFRpGO9fzQw1ms0NDWZz07iGkJmIFH8xxkc3a/WWlubmFkv9AB2SEpDvKxbjidN2faseaNV3zoHXvv7wMODJdkOHAegweAfFPx4G67KluxzottCU9n8CUqXzcIQdXOytAHqXxomvykhEKN9EFutG22p//0rbNvHVxiJywa8yS2KDfV1dfbu31H8jF1RHiTKtWYeHxUvq3bn0pyjCRaiRU6aDO+gb3aEfEeVNsDgm8zzLy9egPa7Qt8TSJdwhjplk06HH43ZNJ3s91KKCHQ5x4sw1fRGYDZ0n1L4FKb9/BP5JLYxToheoFCVxz57PPS8UhhEpLBVeAAAAAElFTkSuQmCC'
  const redStar = 'R0lGODlhEAAQAMQAAORHHOVSKudfOulrSOp3WOyDZu6QdvCchPGolfO0o/XBs/fNwfjZ0frl3/zy7////wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAkAABAALAAAAAAQABAAAAVVICSOZGlCQAosJ6mu7fiyZeKqNKToQGDsM8hBADgUXoGAiqhSvp5QAnQKGIgUhwFUYLCVDFCrKUE1lBavAViFIDlTImbKC5Gm2hB0SlBCBMQiB0UjIQA7'
  try {
    const tom = await createUser({
      name: 'Tom',
      email: 'tom-with-a+really.long_email_that_USES_a_lot_of_symbols@example.test',
      isSighter: true,
      isIdentifier: false,
      isAdmin: false,
    })
    const andrew = await createUser({
      name: 'Andrew',
      email: 'andrew@example.test',
      isIdentifier: false,
    })
    const aPhoto = await createPhoto({
      name: 'photo 1',
      data: chromeLogo,
      type: 'top',
    })
    const sighting = await createIndividualRecord({
      orchidType: 'epiphyte',
      orchidPhotos: [
        await createPhotoSeries({
          top: aPhoto,
          front: aPhoto,
          leaf: aPhoto,
          microHabitat: aPhoto,
        })
      ],
      habitatPhotos: [
        await createPhoto({
          name: 'panoramic landscape',
          data: redStar,
          type: 'habitat'
        })
      ],
      geojsonPoint: {
        type: 'Point',
        coordinates: [138.620, -34.975]
      },
      altitudeMetres: 88,
      locationAccuracy: 100,
      surroundingLanduse: 'intensive',
      isSurroundedByLitter: true,
      hostTreeSpecies: 'Eucalyptus sp.',
      landformElement: 'plain',
      isSoilSandy: false,
      vegetationCommunityNotes: 'lots and lots of tall trees',
    })
    await createCensus({
      name: 'Sunny day orchid hunting #1',
      censusStartDate: new Date('2017-12-17T03:24:00').getTime(),
      type: 'individual',
      individualRecord: sighting,
      deviceInfo: [
        await createDeviceInfo({
          name: 'My phone',
          manufacturer: 'Samsung',
          model: 'S8',
          version: 'Android 7.0',
          displayDensity: 'hd',
          displayDpi: 570,
          displayHeight: 2960,
          displayWidth: 1440,
          logicalDensityFactor: 42,
        })
      ],
      collectedBy: tom,
    })
    await createIdentification({
      determinedName: 'Thatus speciesus',
      eventTimestamp: new Date().getTime(),
      identifiedBy: andrew,
      individualRecord: sighting
    })
  } catch(err) {
    handleError(err)
  }
})();
(async function f2 () {
  try {
    const sally = await createUser({
      name: 'Sally',
      email: 'sally@example.test',
      isSighter: true,
    })
    const sighting = await createPopulationRecord({
      orchidType: 'terrestrial',
      //orchidPhotos: [ // TODO add photo(s)
      //],
      //habitatPhotos: [ // TODO add photo
      //],
      geojsonPolygon: {
        type: 'Polygon',
        coordinates: [
          [
            [137.22805738449097, -35.64899687605032],
            [137.22877621650696, -35.64961587158131],
            [137.2297203540802, -35.64937176124047],
            [137.23029971122742, -35.648674299013415],
            [137.22958087921143, -35.648011704257435],
            [137.2281539440155, -35.64789836513034],
            [137.22805738449097, -35.64899687605032]
          ]
        ]
      },
      altitudeMetres: 12,
      locationAccuracy: 30,
      surroundingLanduse: 'conservation',
      isSurroundedByLitter: true,
      landformElement: 'hill',
      isSoilSandy: false,
      numberOfIndividualsRecorded: 3,
      accuracyOfCount: 'exact',
      vegetationCommunityNotes: getLongDescription(),
    })
    await createCensus({
      name: 'Finding orchid pops',
      censusStartDate: new Date('2016-10-30T05:22:00').getTime(),
      type: 'population',
      populationRecord: sighting,
      deviceInfo: [
        await createDeviceInfo({
          name: 'User McUserface',
          manufacturer: 'LG',
          model: 'V30',
          version: 'Android 8.0',
          displayDensity: 'xxhd',
          displayDpi: 537,
          displayHeight: 2880,
          displayWidth: 1440,
          logicalDensityFactor: 42,
        })
      ],
      collectedBy: sally,
    })
  } catch(err) {
    handleError(err)
  }
})();

// TODO create MappingRecord

function handleError (err) {
  if (!err) {
    return
  }
  throw new VError(err, `Something failed`)
}

async function createCensus (body) {
  const resp = await _createHelper('census', body, 'Census', 'census')
  return resp.id
}

async function createDeviceInfo (body) {
  const resp = await _createHelper('deviceinfo', body, 'Device Info', 'deviceinfo')
  return resp.id
}

async function createIdentification (body) {
  const resp = await _createHelper('identification', body, 'Identification', 'identification')
  return resp.id
}

async function createIndividualRecord (body) {
  const resp = await _createHelper('individualRecord', body, 'Individual record', 'record.individual')
  return resp.id
}

async function createMappingRecord (body) {
  const resp = await _createHelper('mappingRecord', body, 'Mapping record', 'record.mapping')
  return resp.id
}

async function createPhoto (body) {
  const resp = await _createHelper('photo', body, 'Photo', 'photo')
  return resp.id
}

async function createPhotoSeries (body) {
  const resp = await _createHelper('photoseries', body, 'Photo series', 'photoseries')
  return resp.id
}

async function createPopulationRecord (body) {
  const resp = await _createHelper('populationRecord', body, 'Population record', 'record.population')
  return resp.id
}

async function createUser (body) {
  const resp = await _createHelper('user', body, 'User', 'user')
  return resp.id
}

async function _createHelper (urlSuffix, postBody, modelName, mimeFragment) {
  const dontPopulateAssociationsInResponse = '?populate=false'
  let resp
  try {
    resp = await request
      .post(url(`${urlSuffix}`) + dontPopulateAssociationsInResponse)
      .set('Accept', `application/vnd.org.wildorchidwatch.${mimeFragment}.v1+json`)
      .send(postBody)
  } catch (error) {
    const errorBody = error.response && error.response.text || error
    throw new VError(error, `Failed while trying to POST data='${JSON.stringify(postBody, null, 2)}' to /${urlSuffix}. Error response body: '${errorBody}'`)
  }
  let respBody = resp.body
  const id = respBody.id
  console.log(`Created ${modelName} with id=${id}`)
  return resp.body
}

function getLongDescription() {
  return `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare turpis mauris,
  id sagittis felis hendrerit in. Vivamus ac faucibus mauris. Nam quis tempus odio. Praesent
  tempor metus sit amet sem mollis, consequat interdum sapien malesuada. Suspendisse ornare
  vehicula libero eu mollis. Curabitur sed nibh eget enim gravida blandit accumsan vel mi.
  Donec in lacus eros. Maecenas auctor diam nec lectus bibendum viverra. Fusce maximus est
  vitae augue ultricies, ac tempus sapien auctor. Curabitur dictum, magna accumsan porttitor
  imperdiet, ante magna commodo arcu, rhoncus ornare sapien quam quis quam. Cras non mi nunc.
  Nunc a turpis eu mi bibendum vulputate ac non erat. Vestibulum quis erat rhoncus, finibus
  purus at, consequat ex. Fusce feugiat dui ut nulla condimentum pulvinar. Sed aliquet nisi
  in sapien semper, nec sodales urna feugiat. Fusce vestibulum felis sed dignissim venenatis.
  Nullam sed magna non mauris sodales dictum et eu lorem. Quisque dolor ante, commodo lacinia
  efficitur sed, ultrices semper risus. Cras at massa vitae leo ullamcorper pharetra id sit
  amet eros. In aliquam mauris nec sapien fringilla convallis. Sed vehicula sagittis pharetra.
  Duis dictum nunc in urna cursus interdum. Praesent nec faucibus metus. Nunc diam purus,
  feugiat ac ex eu, consectetur dignissim elit. Pellentesque habitant morbi tristique senectus
  et netus et malesuada fames ac turpis egestas.`
}

function url (suffix) {
  return `${URL_PREFIX}/${suffix}`
}

